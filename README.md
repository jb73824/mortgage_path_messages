# Installation

Make sure you have the following installed:
- JRE 1.8
- Kotlin 1.4
- Postgres

Now create the database and the DB user:
`createuser mortgage_path`
`createdb mortgage_path_interview --owner mortgage_path`

To install the dependencies and execute the test suite run:
`./gradlew test`

Then to run the application locally on port 8080.
`./gradlew bootRun`
