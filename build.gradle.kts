import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("org.springframework.boot") version "2.4.4"
  id("io.spring.dependency-management") version "1.0.11.RELEASE"
  id("org.flywaydb.flyway") version "7.7.2"
  id("org.jetbrains.kotlin.plugin.jpa") version "1.5.0-M2"
  kotlin("plugin.allopen") version "1.4.32"
  kotlin("jvm") version "1.4.31"
  kotlin("plugin.spring") version "1.4.31"
}

group = "com.mortgagepath"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
  mavenCentral()
}

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
  implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.4.4")
  implementation( "org.flywaydb:flyway-core:7.7.2")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  implementation("org.postgresql:postgresql:42.2.18")
  implementation("com.google.guava:guava:20.0")

  implementation("com.google.code.gson:gson:2.8.6")
  testImplementation("org.junit.jupiter:junit-jupiter-api")
  testImplementation("org.junit.jupiter:junit-jupiter-engine")
  testImplementation("org.junit.platform:junit-platform-engine:1.7.1")
  testImplementation("org.junit.platform:junit-platform-launcher:1.7.1")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.flywaydb.flyway-test-extensions:flyway-test:7.0.0")
  testImplementation("org.flywaydb.flyway-test-extensions:flyway-spring-test:7.0.0")
  developmentOnly("org.springframework.boot:spring-boot-devtools")
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "1.8"
  }
}

flyway {
  url = "jdbc:postgresql://localhost:5432/mortgage_path_messages"
  user = "mortgage_path"
  password = "pass123"
  driver = "org.postgresql.Driver"
  schemas = arrayOf("public")
}

tasks.withType<Test> {
  useJUnitPlatform()
}

allOpen {
  annotation("javax.persistence.Entity")
  annotation("javax.persistence.Embeddable")
  annotation("javax.persistence.MappedSuperclass")
}
