package com.mortgagepath.messages

import com.google.common.hash.Hashing
import com.google.gson.Gson
import org.flywaydb.core.Flyway
import org.flywaydb.test.annotation.FlywayTest
import org.flywaydb.test.junit5.annotation.FlywayTestExtension
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.nio.charset.StandardCharsets


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@EnableConfigurationProperties
@FlywayTestExtension
@FlywayTest
@Rollback
class MessagesIntegrationTest {
  @Autowired
  lateinit var mockMvc: MockMvc
  @Autowired
  lateinit var messageRepository: MessageRepository
  @Autowired
  lateinit var flyway: Flyway
  var gson: Gson
  constructor() {
    gson = Gson()
  }

  @AfterEach
  fun restoreDatabase() {
    flyway.clean()
    flyway.migrate()
  }

  @Test
  fun `response should have hashed value of input`() {
    val message = "this is a test message"
    val encryptedMessage = Hashing.sha256().hashString(message, StandardCharsets.UTF_8).toString()
    mockMvc.perform(
      MockMvcRequestBuilders.post("/messages")
        .content(gson.toJson(hashMapOf("message" to message)).toString())
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
    )
      .andExpect(status().isOk())
      .andExpect(content().contentType("application/json"))
      .andExpect(jsonPath("$.digest").exists())
      .andExpect(jsonPath("$.digest").value("$encryptedMessage"))
  }

  @Test
  fun `should persist the message`() {
    val message = "this is a test message"
    val encryptedMessage = Hashing.sha256().hashString(message, StandardCharsets.UTF_8).toString()
    mockMvc.perform(
      MockMvcRequestBuilders.post("/messages")
        .content(gson.toJson(hashMapOf("message" to message)).toString())
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
    )
    val messageEntities = messageRepository.findAll()
    assertEquals(1, messageEntities.count())
    val messageEntity = messageEntities.iterator().next()
    assertEquals(encryptedMessage, messageEntity.digest)
    assertEquals(message, messageEntity.message)
  }

  @Test
  fun `should return 422 if the request is ill-formatted`() {
    mockMvc.perform(
      MockMvcRequestBuilders.post("/messages")
        .content("{}")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().`is`(422))
  }

  @Test
  fun `should return 404 if the request body is empty`() {
    mockMvc.perform(
      MockMvcRequestBuilders.post("/messages")
        .content("")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
    ).andExpect(status().`is`(400))
  }

  @Test
  fun `should return the message that corresponds to the given digest`() {
    val message = "this is a test message"
    val encryptedMessage = Hashing.sha256().hashString(message, StandardCharsets.UTF_8).toString()
    messageRepository.save(Message(encryptedMessage, message))
    mockMvc.perform(
      MockMvcRequestBuilders.get("/messages/$encryptedMessage")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
    )
      .andExpect(status().isOk())
      .andExpect(content().contentType("application/json"))
      .andExpect(jsonPath("$.message").exists())
      .andExpect(jsonPath("$.message").value(message))
  }

  @Test
  fun `should return 404 if we could not find a matching record`() {
    mockMvc.perform(
      MockMvcRequestBuilders.get("/messages/23490d903ef")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
    )
      .andExpect(status().isNotFound)
  }
}