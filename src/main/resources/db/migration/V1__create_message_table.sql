create table message (
    id SERIAL PRIMARY KEY,
    digest VARCHAR(100) NOT NULL,
    message TEXT NOT NULL
);

CREATE UNIQUE INDEX messages_digest ON message (digest);
