package com.mortgagepath.messages

import com.google.common.hash.Hashing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.nio.charset.StandardCharsets

@RestController
class MessagesController {
  @Autowired
  lateinit var messageRepository: MessageRepository

  @GetMapping("/messages/{digest}")
  fun show(@PathVariable digest: String): ResponseEntity<Message> {
    val possibleMessage = messageRepository.findByDigest(digest)
    if(possibleMessage.isPresent) {
      return ResponseEntity<Message>(possibleMessage.get(), HttpStatus.OK)
    }
    return ResponseEntity.notFound().build()
  }

  @PostMapping("/messages")
  fun create(@RequestBody payload: Map<String, String>): ResponseEntity<Message> {
    if(payload["message"] == null) {
      return ResponseEntity.unprocessableEntity().build()
    }
    val hash = Hashing.sha256().hashString(payload["message"]!!, StandardCharsets.UTF_8).toString()
    val messageEntity = Message(hash, payload["message"]!!)
    // Assumes success, in production code we would have error handling.
    messageRepository.save(messageEntity)
    return ResponseEntity<Message>(messageEntity, HttpStatus.OK)
  }
}