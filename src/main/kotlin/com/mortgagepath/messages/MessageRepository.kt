package com.mortgagepath.messages

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
public interface MessageRepository : CrudRepository<Message, Long> {
  fun findByDigest(digest: String): Optional<Message>
}