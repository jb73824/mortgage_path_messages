package com.mortgagepath.messages

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class Message(
  var digest: String,
  var message: String,
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null) {
  constructor() : this("", "")
}
